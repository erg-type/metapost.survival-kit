var testing, pad, inReload, inText, inScale, inPad, iframe, group

function scale(value){
	console.log(testing.style)
	testing.style.transform = 'scale(' + value + ')'
}

function rand(){
	return Math.floor((Math.random() * 10000) + 1)
}

function loadLetters(letters){
	var tabLetters = letters.split('')
	testing.innerHTML = ''
	tabLetters.forEach(function(item, i){
		var itemCode = item.charCodeAt(0)
		var ra = rand()
		testing.innerHTML += '<img src="../projet/svg/' + itemCode + '.svg?rand=' + ra + '" />' 
	})
}

document.addEventListener("DOMContentLoaded", (event) => {
	group = location.hash.replace('#', '')
	testing = document.querySelector('#testing')
	pad = document.querySelector('#pad')
	inReload = document.querySelector('#inReload')
	inText = document.querySelector('#inText')
	inScale = document.querySelector('#inScale')
	inPad= document.querySelector('#inPad')

	inReload.addEventListener('click', (event) => {
		loadLetters(inText.value)
	})

	inScale.addEventListener('change', (event) => {
		console.log(inScale.value)
		scale(inScale.value)
	})

	inPad.addEventListener('click', (event) => {
		pad.classList.toggle("hide")
	})

})
