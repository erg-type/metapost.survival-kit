import fontforge
import glob
import lxml.etree as ET



def removeCadra(root, pattern):
    for child in root:
        if child.tag == '{http://www.w3.org/2000/svg}path':
            if child.attrib['style'].startswith(pattern):
                b = child
            try:
                root.remove(b)
            except:
                print('hoo')
    ET.dump(root)
    return ET.tostring(root, encoding='utf8', method='xml').decode()

def buildFont(svgFolder, fontname):
    SVGS = glob.glob(svgFolder)
    ff = fontforge.font()
    for SVG in SVGS:
        key = SVG.split('/')[-1].split('.')[0]
        print('-->',key)
        if key.isdigit() == True:
            with open(SVG, 'rb') as gp:
                treeLet = ET.parse(gp)
            rootLet = treeLet.getroot()
            gwidth = float(rootLet.get('width')) * (1000 / float(rootLet.get('height')))
            gclean = removeCadra(rootLet, 'stroke:rgb(100.000000%,0.000000%,0.000000%);')
            f = open('/tmp/'+key+'.svg', 'w')
            f.write(gclean)
            f.close()
            letter_char = ff.createChar(int(key))
            letter_char.importOutlines('/tmp/'+key+'.svg')
            letter_char.width = int(gwidth)

    ff.fontname = fontname 
    ff.familyname = fontname
    ff.generate('fonts/'+fontname+'.otf')
    ff.generate('fonts/'+fontname+'.ttf')
    ff.generate('fonts/'+fontname+'.sfd')

buildFont('projet/svg/*.svg', 'myfont')




# def buildFont(self, fileInput, fontname='test'):
#
#     SVG_DIR = glob.glob(self.FileOutput+'/*.svg')
#     # SVG_DIR = glob.glob('out-svg-test/*.svg')
#

#     ff = fontforge.font()
#
#     for SVG in SVG_DIR:
#         gkey, f_ext = os.path.splitext(SVG.split('/')[-1])
#
#         if gkey.isdigit() == True:
#             with open(SVG, 'rb') as gp:
#                 treeLet = ET.parse(gp)
#             rootLet = treeLet.getroot()
#             # gwidth = float(rootLet.get('width')) * (1000 / float(rootLet.get('height')))
#             gwidth = float(rootLet.get('width')) * (1000 / float(rootLet.get('height')))
#             print('key = ', gkey,'; width = ', gwidth )
#
#             gclean = self.removeCadra(rootLet, 'stroke:rgb(100.000000%,0.000000%,0.000000%);')
#             f = open('/tmp/'+gkey+'.svg', 'w')
#             f.write(gclean)
#             f.close()
#             letter_char = ff.createChar(int(gkey))
#             letter_char.importOutlines('/tmp/'+gkey+'.svg')
#             letter_char.width = int(gwidth)
#     ff.fontname = fontname 
#     ff.familyname = fontname
#     ff.generate('output-fontes/'+fontname+'.otf')

