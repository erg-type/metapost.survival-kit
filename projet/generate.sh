#!/usr/bin/bash

if [ ! -d "svg/" ]
then
	mkdir svg
fi

if [ $1 = "-loop" ]
then 
	while :
	do
		mpost -interaction=batchmode mp/base.mp
		sleep 3
	done
else
		mpost -interaction=batchmode mp/base.mp
fi
