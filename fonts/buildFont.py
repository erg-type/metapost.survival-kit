import fontforge as ff
import lxml.etree as et
import glob

SVG_DIR=glob.glob('../projet/svg/*.svg')

for g in SVG_DIR:
    gkey = g.split("/")[-1].replace(".svg", "")
    if gkey.isdigit() == True:
        with open(g, 'rt') as gp:
            treeLet = et.parse(gp)
        rootLet = treeLet.getroot()
        gwidth = rootLet.get('width')
        gheight = rootLet.get('height')
        gwidth = round(float(gwidth))
        gclean = removeCadra(g, 'stroke:rgb(100.000000%,0.000000%,0.000000%);')
        pos = getInfoPath(g, 'stroke:rgb(100.000000%,0.000000%,0.000000%);')
        scaleValue = 1000 / pos[3]
        f = open(g, 'w')
        f.write(str(gclean))
        f.close()


